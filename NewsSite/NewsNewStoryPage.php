<?php
    session_start();
    
    require 'database.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainNewStory">
            <h1>Submit a New Story</h1>
            <?php
                if (!$_SESSION["isGuest"])
                {
                    echo "<form id=\"newStoryForm\" action=\"NewsNewStory.php\" method=\"post\">
                    
                        <h3>Enter the title, a link to the original story (optional), and the story text below.</h3>
                        <input type=\"submit\" value=\"Submit Story\">
                        <br>
                        <label>Title:</label> <input type=\"text\" id=\"newTitle\" name=\"newTitle\" maxlength=\"140\"><br>
                        <label>Link:</label> <input type=\"text\" id=\"newLink\" name=\"newLink\" maxlength=\"200\"><br>
                    
                    </form>
                    <label>Story:</label><textarea rows=\"4\" cols=\"50\" name=\"newStory\" maxlength=\"2000\" form=\"newStoryForm\" id=\"newStory\">Enter story here...</textarea><br>";
                }
                else
                {
                    echo "<h3>Guests cannot submit new news stories.</h3>";
                }
            ?>
            
            <!-- Nav Buttons -->
            <form id="navForm" action="NewsNavAction.php" method="post">
                <input type="submit" value="View Latest Stories" name="latestStories">
                <input type="submit" value="View All Stories" name="allStories">
                <?php
                    if (!$_SESSION["isGuest"])
                    {
                        echo "<input type=\"submit\" value=\"View My Stories\" name=\"myStories\">";
                        echo "<input type=\"submit\" value=\"Submit New Story\" name=\"newStory\">";
                    }
                ?>
                <input type="submit" value="Logout" name="logout">
            </form>
        </div>
    </body>
</html>