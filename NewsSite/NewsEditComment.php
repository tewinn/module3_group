<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainLogin">
            <h1>Edit Comment</h1>
            <form id="editCommentForm" action="NewsEditCommentAction.php" method="post">
                
<?php
    if (!$_SESSION["isGuest"])
    {
        printf( "Comment: <input type=\"text\" id=\"editedComment\" value=\"%s\"name=\"editedComment\" maxlength=\"500\">
            <input type=\"hidden\" name=\"storyID\" value=\"%d\"/>
            <input type=\"hidden\" name=\"commentID\" value=\"%d\"/>",
            htmlspecialchars($_POST["commentText"]),
            htmlspecialchars($_POST["storyID"]),
            htmlspecialchars($_POST["commentID"]));
        echo "<input type=\"submit\" value=\"Save Comment\" name=\"submitEditComment\">
            </form>";
    }
    else
    {
        // Guests cannot edit comments
        echo "<h2>ERROR: Guests cannot edit comments; please login or create an account to submit a comment.</h2>
        </form>";        
    }
    
?>
            <!-- Return/cancel editing comment -->
            <form id="editCommentForm" action="NewsViewStory.php" method="post">
                
                    <h4>To return to story and cancel the comment edit, press return.</h4>
                    <?php
                        printf( "<input type=\"hidden\" name=\"id\" value=\"%d\"/>",
                       $_POST["storyID"]);
                    ?>
                    <input type="submit" value="Return"><br>
                
            </form>
        </div>
    </body>
</html>