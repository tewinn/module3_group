<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainNewStory">
                
            <?php
                if (!$_SESSION["isGuest"])
                {
                    // Use a prepared statement to add new comment
                    $stmt = $mysqli->prepare("INSERT INTO stories (author, title, link, story) VALUES (?,?,?,?)");
                    $newTitle = $_POST["newTitle"];
                    $author = $_SESSION["username"];
                    $link = $_POST["newLink"];
                    $story = $_POST["newStory"];
                    if (strcmp(trim($newTitle), null) != 0 &&
                        strcmp(trim($story), null) != 0)
                    {
                        // Bind the parameter
                        $stmt->bind_param('ssss', $author, $newTitle, $link, $story);
            
                        // Execute and check status
                        if($stmt->execute())
                        {
                            // Success!
                           echo "<h2>Story submitted successfully!</h2>";
                        }
                        else
                        {
                            // There was an error
                            echo "<h2>ERROR: There was an issue submitting the story; please try again.</h2>";
                        }
                    }
                    else
                    {
                        // Comment can't be empty
                        echo "<h2>ERROR: The title and story cannot be empty; please try again.</h2>";
                    }
                }
                else
                {
                    // Guests cannot add comments
                    echo "<h2>ERROR: Guests cannot submit stories; please login or create an account to submit a story.</h2>";        
                }
            ?>
            <!-- Nav Buttons -->
            <form id="navForm" action="NewsNavAction.php" method="post">
                <input type="submit" value="View Latest Stories" name="latestStories">
                <input type="submit" value="View All Stories" name="allStories">
                <?php
                    if (!$_SESSION["isGuest"])
                    {
                        echo "<input type=\"submit\" value=\"View My Stories\" name=\"myStories\">";
                        echo "<input type=\"submit\" value=\"Submit New Story\" name=\"newStory\">";
                    }
                ?>
                <input type="submit" value="Logout" name="logout">
            </form>
        </div>
    </body>
</html>