<?php
    session_start();
    $_SESSION["message"] = "";
    if (isset($_POST["allStories"]))
    {
        // Go to all stories
        header('Location: /~tewinn/module3_NewsSite/NewsAllStories.php');
    }
    else if (isset($_POST["latestStories"]))
    {
        // Go to latest stories
        header('Location: /~tewinn/module3_NewsSite/NewsHome.php');
    }
    else if (isset($_POST["myStories"]))
    {
        // Go to user's stories
        header('Location: /~tewinn/module3_NewsSite/NewsMyStories.php');
    }
    else if (isset($_POST["newStory"]))
    {
        // Go to submit a new story
        header('Location: /~tewinn/module3_NewsSite/NewsNewStoryPage.php');
    }
    else
    { // Logout
        // Destroy session
        session_unset();
        session_destroy();
        // Return to login page
        header('Location: /~tewinn/module3_NewsSite/SplashScreen.html');
    }
?>