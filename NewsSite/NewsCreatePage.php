<?php
    session_start();
    
    require 'database.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site Create Account</title>
    </head>
    <body>
        <div id="mainCreate">
            <form id="createForm" action="NewsCreate.php" method="post">
                <h1>
                    Create a New Account
                </h1>
                <br>
                    <h3>Enter a username, your name, email, and password below.</h3>
                    <?php
                        echo $_SESSION["message"];
                    ?>
                    <br>
                    Username: <input type="text"  name="username"><br>
                    Name: <input type="text" name="name"><br>
                    Email: <input type="text" name="email"><br>
                    Password: <input type="password" name="password"><br>
                    <input type="submit" value="Submit"><br>
                
            </form>
        </div>
    </body>
</html>