<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainEdit">
            <form id="returnForm" action="NewsMyStories.php" method="post">
                
<?php
    if (!$_SESSION["isGuest"])
    {
        // Use a prepared statement to edit/update story
        $stmt = $mysqli->prepare("UPDATE stories SET title=?, link=?, story=? WHERE id=? AND author=?");
        $editTitle = $_POST["editTitle"];
        $editLink = $_POST["editLink"];
        $editStory = $_POST["editStory"];
        $id = $_POST["storyID"];
        $author = $_SESSION["username"];
        if (strcmp(trim($editTitle), null) != 0 &&
            strcmp(trim($editStory), null) != 0)
        {
            // Bind the parameter
            $stmt->bind_param('sssis', $editTitle, $editLink, $editStory, $id, $author);

            // Execute and check status
            if($stmt->execute())
            {
                // Success!
               echo "<h2>Story saved successfully!</h2>";
            }
            else
            {
                // There was an error
                echo "<h2>ERROR: There was an issue saving the story; please try again.</h2>";
            }
        }
        else
        {
            // Comment can't be empty
            echo "<h2>ERROR: The story cannot be empty; please try again.</h2>";
        }
    }
    else
    {
        // Guests cannot edit comments
        echo "<h2>ERROR: Guests cannot edit stories; please login or create an account to edit a story.</h2>";        
    }
    
?>
                
                    <h3>To return to user stories, press return.</h3>
                    <?php
                        printf( "<input type=\"hidden\" name=\"id\" value=\"%d\"/><br>",
                       $_POST["storyID"]);
                    ?>
                    <input type="submit" value="Return"><br>
                
            </form>
        </div>
    </body>
</html>