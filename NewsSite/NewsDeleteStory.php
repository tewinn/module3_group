<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainEdit">
            <form id="returnForm" action="NewsMyStories.php" method="post">
                
<?php
    if (!$_SESSION["isGuest"])
    {
        // Use a prepared statement to delete all comments with story ID
        $stmt = $mysqli->prepare("DELETE FROM comments WHERE storyID=?");
        $id = $_POST["storyID"];
        
        // Bind the parameter
        $stmt->bind_param('i', $id);
        
        // Execute and check status
        if($stmt->execute())
        {
            // Success! Don't print anything; close statement and prep to delete the story
            $stmt->close();
            
            // Use a prepared statement to delete story
            $stmt2 = $mysqli->prepare("DELETE FROM stories WHERE id=? AND author=?");
            $id = $_POST["storyID"];
            $author = $_SESSION["username"];
            
            // Bind the parameter
            $stmt2->bind_param('is', $id, $author);
            
            // Execute and check status
            if($stmt2->execute())
            {
                // Success!
                echo "<h2>Story deleted successfully!</h2>";
            }
            else
            {
                // There was an error
                echo "<h2>ERROR: There was an issue deleting the story; please try again.</h2>";
            }
            $stmt2->close();    
        }
        else
        {
            // There was an error
            echo "<h2>ERROR: There was an issue deleting all corresponding comments; please try again.</h2>";
            $stmt->close();
        }
    }
    else
    {
        // Guests cannot edit comments
        echo "<h2>ERROR: Guests cannot delete stories; please login or create an account to delete your stories.</h2>";        
    }
    
?>
                
                    <h3>To return to user stories, press return.</h3>
                    <?php
                        printf( "<input type=\"hidden\" name=\"id\" value=\"%d\"/><br>",
                       $_POST["storyID"]);
                    ?>
                    <input type="submit" value="Return"><br>
                
            </form>
        </div>
    </body>
</html>