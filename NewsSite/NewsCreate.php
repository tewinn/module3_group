<?php
    session_start();
    
    require 'database.php';
    
    // Check for empty values
    $user = $_POST['username'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    if (strcmp(trim($user), null) == 0 ||
        strcmp(trim($name), null) == 0 ||
        strcmp(trim($email), null) == 0 ||
        strcmp(trim($password), null) == 0)
    {
            // Account fields cannot be empty
            $_SESSION["message"] = "ERROR: Fields cannot be empty; please try again."; 
             header('Location: /~tewinn/module3_NewsSite/NewsCreatePage.php');
             break;
    }
    
    
    // Use a prepared statement to check for user
    $stmt = $mysqli->prepare("SELECT COUNT(*), username FROM users WHERE username=?");

    // Bind the parameter
    $user = $_POST['username'];
    $stmt->bind_param('s', $user);
    $stmt->execute();
    
    // Bind the results
    $stmt->bind_result($cnt, $user);
    $stmt->fetch();
    
    if ($cnt == 1)
    {
        // That username already exists; redirect to create account screen with error message
        $_SESSION["message"] = "ERROR: Username already taken; please try again."; 
        header('Location: /~tewinn/module3_NewsSite/NewsCreatePage.php');
        $stmt->close();
    }
    else
    {
        // Close prior statement to begin new statement
        $stmt->close();
        
        // Use a prepared statement to create user
        $user = $_POST['username'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $password = password_hash($password, PASSWORD_BCRYPT);
        $stmt2 = $mysqli->prepare("INSERT INTO users VALUES (?, ?, ?, ?)");
        $stmt2->bind_param('ssss', $user, $name, $email, $password);
        if ($stmt2->execute())
        {
            // Account creation succeded!
            $_SESSION['username'] = $user;
            $_SESSION['name'] = $name;
            $_SESSION['email'] = $email;
            $_SESSION["isGuest"] = false;
            // Redirect to News Home page
            $_SESSION["message"] = ""; 
            header('Location: /~tewinn/module3_NewsSite/NewsHome.php');
        }
        else
        {
            
            // Something went wrong
            $_SESSION["message"] = "ERROR: Something went wrong with account creation; please try again."; 
            header('Location: /~tewinn/module3_NewsSite/NewsCreatePage.php');
        }
    }
    
?>