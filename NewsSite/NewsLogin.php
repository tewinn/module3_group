<?php
    session_start();
    
    require 'database.php';

    // Use a prepared statement to find user in users database
    $stmt = $mysqli->prepare("SELECT COUNT(*), username, name, email, password FROM users WHERE username=?");

    // Bind the parameter
    $stmt->bind_param('s', $user);
    $user = $_POST['username'];
    $stmt->execute();
    
    // Bind the results
    $stmt->bind_result($cnt, $user, $name, $email, $pwd_hash);
    $stmt->fetch();
    
    $pwd_guess = $_POST['password'];
    
    // Compare the submitted password to the actual password hash
    if($cnt == 1 && password_verify($pwd_guess, $pwd_hash))
    {
        // Login succeeded!
        $_SESSION['username'] = $user;
        $_SESSION['name'] = $name;
        $_SESSION['email'] = $email;
        $_SESSION["isGuest"] = false;
        // Redirect to News Home page
        $_SESSION["message"] = "";
        header('Location: /~tewinn/module3_NewsSite/NewsHome.php');
    }
    else
    {
        // Login failed; redirect back to the login screen with error message
        $_SESSION["message"] = "ERROR: Username or password is incorrect; please try again.";
        header('Location: /~tewinn/module3_NewsSite/NewsLoginPage.php');
    }
    
?>