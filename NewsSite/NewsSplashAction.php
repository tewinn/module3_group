<?php
    session_start();
    if(isset($_SESSION["username"]))
    {
        unset($_SESSION["username"]);
    }
    
    // Check type of submit to direct correctly
    if (isset($_POST["login"]))
    {
        // Directs to  user login screen
        $_SESSION["message"] = ""; 
        header('Location: /~tewinn/module3_NewsSite/NewsLoginPage.php');
    }
    else if (isset($_POST["createAccount"]))
    {
        // Directs to create an accout screen
        $_SESSION["message"] = ""; 
        header('Location: /~tewinn/module3_NewsSite/NewsCreatePage.php');
    }
    else
    {
        $_SESSION["username"] = "guest";
        $_SESSION["name"] = "Guest";
        $_SESSION['email'] = "none";
        $_SESSION["isGuest"] = true;
        // Directs to news site home as a guest
        header('Location: /~tewinn/module3_NewsSite/NewsHome.php');
    }
    
?>