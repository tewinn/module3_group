<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainEdit">
            <form id="returnForm" action="NewsViewStory.php" method="post">
                
<?php
    if (!$_SESSION["isGuest"])
    {
        // Use a prepared statement to edit/update comment
        $stmt = $mysqli->prepare("UPDATE comments SET comment=? WHERE id=? AND storyID=? AND author=?");
        $id = $_POST["commentID"];
        $storyID = $_POST["storyID"];
        $commentText = $_POST["editedComment"];
        $author = $_SESSION["username"];
        if (strcmp(trim($commentText), null) != 0)
        {
            // Bind the parameter
            $stmt->bind_param('siis', $commentText, $id, $storyID, $author);

            // Execute and check status
            if($stmt->execute())
            {
                // Success!
               echo "<h2>Comment saved successfully!</h2>";
            }
            else
            {
                // There was an error
                echo "<h2>ERROR: There was an issue saving the comment; please try again.</h2>";
            }
        }
        else
        {
            // Comment can't be empty
            echo "<h2>ERROR: The comment cannot be empty; please try again.</h2>";
        }
    }
    else
    {
        // Guests cannot edit comments
        echo "<h2>ERROR: Guests cannot edit comments; please login or create an account to edit a comment.</h2>";        
    }
    
?>
            
                    <h3>To return to story, press return.</h3>
                    <?php
                        printf( "<input type=\"hidden\" name=\"id\" value=\"%d\"/><br>",
                       $_POST["storyID"]);
                    ?>
                    <input type="submit" value="Return"><br>
                
            </form>
        </div>
    </body>
</html>