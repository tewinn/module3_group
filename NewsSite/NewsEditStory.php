<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainLogin">
            <h1>Edit Comment</h1>
                
<?php
    if (!$_SESSION["isGuest"])
    {
        $storyTitle = $_POST["title"];
        $storyLink = $_POST["link"];
        $storyText = $_POST["story"];
        echo "<form id=\"editStoryForm\" action=\"NewsEditStoryAction.php\" method=\"post\">
                    
                        <input type=\"submit\" value=\"Save Story\">
                        <br>
                        <label>Title:</label> <input type=\"text\" id=\"editTitle\" name=\"editTitle\" maxlength=\"140\" value=\"$storyTitle\"><br>
                        <label>Link:</label> <input type=\"text\" id=\"editLink\" name=\"editLink\" maxlength=\"200\" value=\"$storyLink\"><br>";
        printf( "<input type=\"hidden\" name=\"author\" value=\"%s\"/>
            <input type=\"hidden\" name=\"storyID\" value=\"%d\"/>",
            htmlspecialchars($_SESSION["username"]),
            htmlspecialchars($_POST["storyID"]));
        echo "</form>
            <label>Story:</label><textarea rows=\"4\" cols=\"50\" name=\"editStory\" maxlength=\"2000\" form=\"editStoryForm\" id=\"editStory\">$storyText</textarea><br>";
    }
    else
    {
        // Guests cannot edit comments
        echo "<h2>ERROR: Guests cannot edit comments; please login or create an account to submit a comment.</h2>";        
    }
    
?>
            <!-- Return/cancel editing story -->
            <form id="editStoryFormReturn" action="NewsMyStories.php" method="post">
                
                    <h4>To return to user stories and cancel the story edit, press return.</h4>
                    <input type="submit" value="Return"><br>
                
            </form>
        </div>
    </body>
</html>