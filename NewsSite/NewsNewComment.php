<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainNewComment">
            <form id="returnForm" action="NewsViewStory.php" method="post">
                
<?php
    if (!$_SESSION["isGuest"])
    {
        // Use a prepared statement to add new comment
        $stmt = $mysqli->prepare("INSERT INTO comments (storyID, author, comment) VALUES (?,?,?)");
        $storyID = $_POST["storyID"];
        $user = $_SESSION["username"];
        $comment = $_POST["newComment"];
        if (strcmp(trim($comment), null) != 0)
        {
            // Bind the parameter
            $stmt->bind_param('iss', $storyID, $user, $comment);

            // Execute and check status
            if($stmt->execute())
            {
                // Success!
               echo "<h2>Comment submitted successfully!</h2>";
            }
            else
            {
                // There was an error
                echo "<h2>ERROR: There was an issue submitting the comment; please try again.</h2>";
            }
        }
        else
        {
            // Comment can't be empty
            echo "<h2>ERROR: The comment cannot be empty; please try again.</h2>";
        }
    }
    else
    {
        // Guests cannot add comments
        echo "<h2>ERROR: Guests cannot submit comments; please login or create an account to submit a comment.</h2>";        
    }
    
?>
                
                    <h3>To return to story, press return.</h3>
                    <?php
                        printf( "<input type=\"hidden\" name=\"id\" value=\"%d\"/><br>",
                       $_POST["storyID"]);
                    ?>
                    <input type="submit" value="Return"><br>
                
            </form>
        </div>
    </body>
</html>