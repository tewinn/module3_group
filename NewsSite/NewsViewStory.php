<?php
    session_start();
    require 'database.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainStory">
            
            <?php
                echo $_SESSION["message"];
                // Use a prepared statement
                $stmt = $mysqli->prepare("SELECT * FROM stories WHERE id=?");
                // Use value from hidden form field
                $stmt->bind_param('i', $_POST["id"]);
                $stmt->execute();
                // Bind the results
                $stmt->bind_result($id, $author, $title, $link, $story);
                // Display story;
                $stmt->fetch();
                echo "<div class=\"newsStory\">";
                printf("<h2>%s</h2>", htmlspecialchars($title));
                if(strcmp(trim(htmlspecialchars($link)),null) != 0)
                {
                    printf("<a href=\"%s\">Original Story</a><br>", htmlspecialchars($link));
                }
                printf("<p class=\"storyBody\">Posted by: %s<br>
                    %s</p><br>",
                    htmlspecialchars($author),
                    htmlspecialchars($story)
                );
                // Close statement to begin next statement
                $stmt->close();
                
                // Get all comments associated with story
                $stmt2 = $mysqli->prepare("SELECT * FROM comments WHERE storyID=?");
                // Use value from hidden form field
                $stmt2->bind_param('i', $_POST["id"]);
                $stmt2->execute();
                // Bind the results
                $stmt2->bind_result($id, $storyID, $author, $comment);
                // Display all comments associated with story
                echo "<h3>Comments:</h3>";
                while ($stmt2->fetch())
                {
                    printf("<p id=\"commentBody\">Posted by: %s<br>
                        %s<br>",
                        htmlspecialchars($author),
                        htmlspecialchars($comment)
                        );
                    
                    if (strcmp($author, $_SESSION["username"]) == 0)
                    {
                        // Edit comment form
                        echo "<form class=\"commentEditForm\" action=\"NewsEditComment.php\" method=\"post\">";
                        printf( "<input type=\"hidden\" name=\"storyID\" value=\"%d\"/>
                               <input type=\"hidden\" name=\"commentID\" value=\"%d\"/>
                               <input type=\"hidden\" name=\"commentText\" value=\"%s\"/>",
                            htmlspecialchars($_POST["id"]),
                            htmlspecialchars($id),
                            htmlspecialchars($comment));
                        echo "<input type=\"submit\" value=\"Edit Comment\" name=\"editComment\">
                            </form>";
                        // Delete comment form
                        echo "<form id=\"commentDeleteForm\" action=\"NewsDeleteCommentAction.php\" method=\"post\">";
                        printf( "<input type=\"hidden\" name=\"storyID\" value=\"%d\"/>
                               <input type=\"hidden\" name=\"commentID\" value=\"%d\"/>",
                            htmlspecialchars($_POST["id"]),
                            htmlspecialchars($id));
                        echo "<input type=\"submit\" value=\"Delete Comment\" name=\"deleteComment\">
                            </form>";
                        
                    }
                    echo "</p><br>";
                }
                $stmt2->close();
                
                // Submit a comment if not a guest
                if (!$_SESSION["isGuest"])
                {
                    echo "<p><h3>New Comment:</h3>";
                    echo "<form id=\"commentForm\" action=\"NewsNewComment.php\" method=\"post\">";
                    echo "Comment: <input type=\"text\" id=\"newComment\" name=\"newComment\" maxlength=\"500\">";
                    printf( "<input type=\"hidden\" name=\"storyID\" value=\"%d\"/><br>",
                       $_POST["id"]);
                    echo "<input type=\"submit\" value=\"Add Comment\" name=\"addComment\">
                        </form>";
                }
                echo "</div><br>";
            ?>
            <!-- Nav Buttons -->
            <form id="navForm" action="NewsNavAction.php" method="post">
                <input type="submit" value="View Latest Stories" name="latestStories">
                <input type="submit" value="View All Stories" name="allStories">
                <?php
                    if (!$_SESSION["isGuest"])
                    {
                        echo "<input type=\"submit\" value=\"View My Stories\" name=\"myStories\">";
                        echo "<input type=\"submit\" value=\"Submit New Story\" name=\"newStory\">";
                    }
                ?>
                
                <input type="submit" value="Logout" name="logout">
            </form>
        </div>
    </body>
</html>