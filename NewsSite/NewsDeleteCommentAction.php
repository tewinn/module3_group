<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainEdit">
            <form id="returnForm" action="NewsViewStory.php" method="post">
                
<?php
    if (!$_SESSION["isGuest"])
    {
        // Use a prepared statement to delete comment
        $stmt = $mysqli->prepare("DELETE FROM comments WHERE id=? AND storyID=?");
        $id = $_POST["commentID"];
        $storyID = $_POST["storyID"];
        // Bind the parameter
        $stmt->bind_param('ii', $id, $storyID);

        // Execute and check status
        if($stmt->execute())
        {
            // Success!
            echo "<h2>Comment delete successfully!</h2>";
        }
        else
        {
            // There was an error
            echo "<h2>ERROR: There was an issue deleting the comment; please try again.</h2>";
        }
    }
    else
    {
        // Guests cannot edit comments
        echo "<h2>ERROR: Guests cannot delete comments; please login or create an account to delete a comment.</h2>";        
    }
    
?>
                
                    <h3>To return to story, press return.</h3>
                    <?php
                        printf( "<input type=\"hidden\" name=\"id\" value=\"%d\"/><br>",
                       $_POST["storyID"]);
                    ?>
                    <input type="submit" value="Return"><br>
                
            </form>
        </div>
    </body>
</html>