<?php
    session_start();
    require 'database.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainSearch">
            <h1 id="searchedStories">Search Stories</h1>
            <?php
                // First creative portion: Search story content and display stories
                // Form for searching story title and contents
                    echo "<div><form id=\"searchForm\" action=\"NewsSearchStory.php\" method=\"post\">
                        Select a news story field and then enter a term to search:<br>
                        <label>Title</label><input type=\"radio\" name=\"searchType\" value=\"title\">
                        <label>Link</label><input type=\"radio\" name=\"searchType\" value=\"link\">
                        <label>Author</label><input type=\"radio\" name=\"searchType\" value=\"author\">
                        <label>Story Content</label><input type=\"radio\" name=\"searchType\" value=\"story\"><br>
                        <input type=\"text\" name=\"searchStoryText\">
                        <input type=\"submit\" value=\"Search All Stories\" name=\"searchStory\">
                        </form></div><br>";
            
                // Use a prepared statement
                $searchType = $_POST["searchType"];
                $search = $_POST["searchStoryText"];
                $stmt = $mysqli->prepare("SELECT * FROM stories WHERE $searchType LIKE '%$search%' ORDER BY id DESC");
                $stmt->execute();
                // Bind the results
                $stmt->bind_result($id, $author, $title, $link, $story);
                // Loop through the searched stories and display them;
                while($stmt->fetch())
                {
                    echo "<div class=\"newsStory\">";
                    printf("<h4>%s</h4>", htmlspecialchars($title));
                    if(strcmp(trim(htmlspecialchars($link)),null) != 0)
                    {
                        printf("<a href=\"%s\">Original Story</a><br>", htmlspecialchars($link));
                    }
                    printf("<p class=\"storyBody\">Posted by: %s<br>
                        %s</p><br>",
                        htmlspecialchars($author),
                        htmlspecialchars($story)
                    );
                    // Form for viewing story with all comments
                    printf ("<form class=\"storyForm\" action=\"NewsViewStory.php\" method=\"post\">
                            <input type=\"hidden\" name=\"id\" value=\"%d\" />
                            <input type=\"submit\" id=\"%d\" value=\"View Story with All Comments\" name=\"viewStory\">
                            </form>",
                            htmlspecialchars($id),
                            htmlspecialchars($id));
                    echo "</div><br>";
                }
                $stmt->close();
            ?>
            <!-- Nav Buttons -->
            <form id="navForm" action="NewsNavAction.php" method="post">
                <input type="submit" value="View Latest Stories" name="latestStories">
                <?php
                    if (!$_SESSION["isGuest"])
                    {
                        echo "<input type=\"submit\" value=\"View My Stories\" name=\"myStories\">";
                        echo "<input type=\"submit\" value=\"Submit New Story\" name=\"newStory\">";
                    }
                ?>
                <input type="submit" value="Logout" name="logout">
            </form>
        </div>
    </body>
</html>