<?php
    session_start();
    
    require 'database.php';
    ?>
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site Login</title>
    </head>
    <body>
        <div id="mainLogin">
            <form id="loginForm" action="NewsLogin.php" method="post">
                <h1>Login</h1>
                    <h3>Enter your username and password below.</h3>
                    <?php
                        // Prints potential error message
                        echo $_SESSION["message"];
                    ?>
                    <!-- Gathers username and password to submit -->
                    <br>
                    Username: <input type="text" name="username">
                    Password: <input type="password" name="password"><br><br>
                    <input type="submit" value="Submit"><br>
            </form>
        </div>
    </body>
</html>