<?php
    session_start();
    require 'database.php';
?>

<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="NS.css" />
        <title>News Site</title>
    </head>
    <body>
        <div id="mainMine">
            <h1 id="welcome"><?php echo $_SESSION["name"] ?>'s Stories!</h1>
            <?php
                // First creative portion: Search story content and display stories
                // Form for searching story title and contents
                    echo "<div><form id=\"searchForm\" action=\"NewsSearchStory.php\" method=\"post\">
                        Select a news story field and then enter a term to search:<br>
                        <label>Title</label><input type=\"radio\" name=\"searchType\" value=\"title\">
                        <label>Link</label><input type=\"radio\" name=\"searchType\" value=\"link\">
                        <label>Author</label><input type=\"radio\" name=\"searchType\" value=\"author\">
                        <label>Story Content</label><input type=\"radio\" name=\"searchType\" value=\"story\"><br>
                        <input type=\"text\" name=\"searchStoryText\">
                        <input type=\"submit\" value=\"Search All Stories\" name=\"searchStory\">
                        </form></div><br>";
            
                if (!$_SESSION["isGuest"])
                {
                    // Use a prepared statement
                    $stmt = $mysqli->prepare("SELECT * FROM stories WHERE author=? ORDER BY id DESC");
                    $stmt->bind_param('s', $_SESSION["username"]);
                    $stmt->execute();
                    // Bind the results
                    $stmt->bind_result($id, $author, $title, $link, $story);
                    // Loop through the all stories and display them;
                    while($stmt->fetch())
                    {
                        echo "<div class=\"newsStory\">";
                        printf("<h4>%s</h4>", htmlspecialchars($title));
                        if(strcmp(trim(htmlspecialchars($link)),null) != 0)
                        {
                            printf("<a href=\"%s\">Original Story</a><br>", htmlspecialchars($link));
                        }
                        printf("<p class=\"storyBody\">%s</p><br>",
                            htmlspecialchars($story)
                        );
                        // Form for viewing story with all comments
                        printf ("<form class=\"storyForm\" action=\"NewsViewStory.php\" method=\"post\">
                                <input type=\"hidden\" name=\"id\" value=\"%d\" />
                                <input type=\"submit\" value=\"View Story with All Comments\" name=\"viewStory\">
                                </form>",
                                htmlspecialchars($id));
                        // Form for editing story
                        printf ("<form class=\"storyEditForm\" action=\"NewsEditStory.php\" method=\"post\">
                                <input type=\"hidden\" name=\"storyID\" value=\"%d\" />
                                <input type=\"hidden\" name=\"title\" value=\"%s\" />
                                <input type=\"hidden\" name=\"link\" value=\"%s\" />
                                <input type=\"hidden\" name=\"story\" value=\"%s\" />
                                <input type=\"submit\" value=\"Edit Story\" name=\"editStory\">
                                </form>",
                                htmlspecialchars($id),
                                htmlspecialchars($title),
                                htmlspecialchars($link),
                                htmlspecialchars($story));
                        // Form for deleting story with all comments
                        printf ("<form class=\"storyDeleteForm\" action=\"NewsDeleteStory.php\" method=\"post\">
                                <input type=\"hidden\" name=\"storyID\" value=\"%d\" />
                                <input type=\"submit\" value=\"Delete Story\" name=\"deleteStory\">
                                </form>",
                                htmlspecialchars($id));
                        echo "</div><br>";
                    }
                    $stmt->close();
                }
                else
                {
                    echo "<h4>Guest's cannot submit stories. Please login or create an account by clicking the logout button.</h4>";
                }
            ?>
            <!-- Nav Buttons -->
            <form id="navForm" action="NewsNavAction.php" method="post">
                <input type="submit" value="View Latest Stories" name="latestStories">
                <input type="submit" value="View All Stories" name="allStories">
                <?php
                    if (!$_SESSION["isGuest"])
                    {
                        echo "<input type=\"submit\" value=\"Submit New Story\" name=\"newStory\">";
                    }
                ?>
                <input type="submit" value="Logout" name="logout">
            </form>
        </div>
    </body>
</html>